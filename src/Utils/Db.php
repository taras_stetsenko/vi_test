<?php

namespace Utils;

use \Doctrine\DBAL\DriverManager;
use \Doctrine\DBAL\Configuration;
use \Doctrine\DBAL\Connection;
use Symfony\Component\Yaml\Yaml;

class Db
{
    public static function getConnection() : Connection
    {
        return DriverManager::getConnection(
            self::getConfig(),
            new Configuration()
        );
    }

    private static function getConfig() : array
    {
        $config = Yaml::parseFile(__DIR__ . '/../../config.yaml');
        return [
            'dbname' => $config['db']['database'],
            'user' => $config['db']['user'],
            'password' => $config['db']['password'],
            'host' => $config['db']['host'],
            'driver' => 'pdo_mysql',
        ];
    }
}
