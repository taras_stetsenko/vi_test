<?php

namespace Utils;

class Bootstrap
{
    public static function initDb() : void
    {
        $sqls = [];
        $sqls[] = 'CREATE TABLE `products` (
            `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
            `name` VARCHAR(127) NOT NULL,
            `price` DECIMAL(9, 2) NOT NULL,
            PRIMARY KEY (`id`),
            UNIQUE KEY (`name`)
        ) ENGINE=INNODB DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci';
        $sqls[] = 'CREATE TABLE `orders_status_types` (
            `id` TINYINT UNSIGNED NOT NULL AUTO_INCREMENT,
            `name` VARCHAR(31) NOT NULL,
            PRIMARY KEY (`id`),
            UNIQUE KEY (`name`)
        ) ENGINE=INNODB DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci';
        $sqls[] = 'CREATE TABLE `orders` (
            `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
            `status` TINYINT UNSIGNED NOT NULL,
            PRIMARY KEY (`id`),
            KEY (`status`),
            FOREIGN KEY `fk__orders__status` (`status`)
                REFERENCES `orders_status_types`(`id`)
                ON DELETE RESTRICT
                ON UPDATE RESTRICT
        ) ENGINE=INNODB DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci';
        $sqls[] = 'CREATE TABLE `orders_products` (
            `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
            `order_id` BIGINT UNSIGNED NOT NULL,
            `product_id` BIGINT UNSIGNED NOT NULL,
            PRIMARY KEY (`id`),
            UNIQUE KEY (`order_id`, `product_id`),
            FOREIGN KEY `fk__orders_products__order_id` (`order_id`)
                REFERENCES `orders`(`id`)
                ON DELETE RESTRICT
                ON UPDATE RESTRICT,
            FOREIGN KEY `fk__orders_products__product_id` (`product_id`)
                REFERENCES `products`(`id`)
                ON DELETE RESTRICT
                ON UPDATE RESTRICT
        ) ENGINE=INNODB DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci';
        $sqls[] = 'INSERT INTO
            `orders_status_types`
                (`name`)
            VALUES
                (\'new\'),
                (\'paid\')';

        $conn = Db::getConnection();
        foreach ($sqls as $sql) {
            $stmt = $conn->prepare($sql);
            $stmt->execute([]);
        }
    }
}
