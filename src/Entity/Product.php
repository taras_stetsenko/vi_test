<?php

namespace Entity;

class Product
{
    private $id;
    private $name;
    private $price;

    public function __construct(int $id = null, string $name, float $price)
    {
        if ($name === null) {
            throw new \Exception('$name must be not null');
        }
        if ($price === null) {
            throw new \Exception('$price must be not null');
        }
        if ($price < 0) {
            throw new \Exception('$price must not be less than zero');
        }
        $this->id = $id;
        $this->name = $name;
        $this->price = $price;
    }

    public function getId() : int
    {
        return $this->id;
    }

    public function getName() : string
    {
        return $this->name;
    }

    public function getPrice() : float
    {
        return $this->price;
    }
}
