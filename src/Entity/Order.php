<?php

namespace Entity;

class Order
{
    private $id;
    private $status;
    private $products;
    private static $availableStatuses = [
        'new',
        'paid'
    ];

    public function __construct(int $id = null, string $status = 'new', array $products = [])
    {
        if (!in_array($status, self::$availableStatuses)) {
            throw new \Exception(
                '$status must be one of ' .
                implode(',', self::$availableStatuses) . '; ' .
                $status === null ? 'null' : '"' . $status . '" given'
            );
        }
        if ($status == 'paid' && empty ($products)) {
            throw new \Exception(
                'paid order must contain at least one product'
            );
        }
        foreach ($products as $product) {
            if (!($product instanceof Product)) {
                throw new \Exception('Unexpected item in $products');
            }
        }
        $this->id = $id;
        $this->status = $status;
        $this->products = $products;
    }

    public function getId() : int
    {
        return $this->id;
    }

    public function getStatus() : string
    {
        return $this->status;
    }

    public function getProducts() : array
    {
        return $this->products;
    }

    public function getPrice() : float
    {
        $res = 0;
        foreach ($this->products as $product) {
            $res += $product->getPrice();
        }
        return $res;
    }
}
