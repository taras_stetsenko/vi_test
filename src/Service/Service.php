<?php

namespace Service;

use Entity\Order;
use Entity\Product;
use Repository\Repository;

class Service
{
    private $repository;

    public function __construct()
    {
        $this->repository = new Repository();
    }

    public function generateProducts() : array
    {
        $this->repository->generateProducts();
        return $this->getAllProductsAsArray();
    }

    public function getAllProductsAsArray() : array
    {
        $res = [];
        foreach ($this->repository->getAllProducts() as $product) {
            $res[] = $product->toArray();
        }
        return $res;
    }

    public function createOrder(array $productIds) : array
    {
        return $this->orderToArray(
            $this->repository->getOrderById(
                $this->repository->createOrder($productIds)
            )
        );
    }

    public function getOrder(int $orderId) : array
    {
        return $this->orderToArray(
            $this->repository->getOrderById($orderId)
        );
    }

    public function payOrder(int $orderId, float $price) : array
    {
        $order = $this->repository->getOrderById($orderId);
        if (round($order->getPrice(), 2) !== round($price, 2)) {
            throw new \Exception('Wrong price');
        }
        if ($this->pingYaRu()) {
            $this->repository->updateOrderStatus($orderId, 'paid');
            return $this->getOrder($orderId);
        }
        else {
            throw new \Exception('Could not ping ya.ru');
        }
    }

    private function pingYaRu() : bool
    {
        $ch = curl_init('https://ya.ru');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_exec($ch);
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        return $code == 200;
    }

    private function productToArray(Product $product) : array
    {
        return [
            'id' => $product->getId(),
            'name' => $product->getName(),
            'price' => $product->getPrice()
        ];
    }

    private function orderToArray(Order $order) : array
    {
        $res = [
            'id' => $order->getId(),
            'status' => $order->getStatus(),
            'price' => $order->getPrice(),
            'products' => []
        ];
        foreach ($order->getProducts() as $product) {
            $res['products'][] = $this->productToArray($product);
        }
        return $res;
    }
}
