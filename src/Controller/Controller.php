<?php

namespace Controller;

use Service\Service;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class Controller
{
    private $service;

    public function __construct()
    {
        $this->service = new Service();
    }

    public function generateProducts(Request $request) : JsonResponse
    {
        return new JsonResponse(
            $this->service->generateProducts(),
            200
        );
    }

    public function getAllProducts(Request $request) : JsonResponse
    {
        return new JsonResponse(
            $this->service->getAllProducts(),
            200
        );
    }

    public function createOrder(array $params) : JsonResponse
    {
        return new JsonResponse(
            $this->service->createOrder(
                explode(',', $params['product_ids'])
            ),
        200);
    }

    public function payOrder(array $params) : JsonResponse
    {
        return new JsonResponse(
            $this->service->payOrder(
                $params['order_id'],
                str_replace('_', '.', $params['order_price'])
            ),
            200
        );
    }
}
