<?php

namespace Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\Route;

class Router
{
    private $routes;

    public function __construct()
    {
        $this->routes = new RouteCollection();
        $this->routes->add(
            'generate_products_route',
            new Route(
                '/generate_products',
                [
                    'controller' => 'Controller\\Controller',
                    'method'=>'generateProducts'
                ]
            )
        );
        $this->routes->add(
            'get_all_products_route',
            new Route(
                '/get_all_products',
                [
                    'controller' => 'Controller\\Controller',
                    'method'=>'getAllProducts'
                ]
            )
        );
        $this->routes->add(
            'create_order_route',
            new Route(
                '/create_order/{product_ids}',
                [
                    'controller' => 'Controller\\Controller',
                    'method'=>'createOrder'
                ],
                [
                    'product_ids' => '^[1-9]{1}[0-9]{0,9}((,([1-9]{1}))[0-9]{0,9}){0,9}$'
                ]
            )
        );
        $this->routes->add(
            'pay_order_route',
            new Route(
                '/pay_order/{order_id}/{order_price}',
                [
                    'controller' => 'Controller\\Controller',
                    'method'=>'payOrder'
                ],
                [
                    'order_id' => '^[1-9]{1}[0-9]{0,9}$',
                    'order_price' => '^([1-9]{1,}[0-9]{1}(\_[0-9]{2}){0,1})|(0\_(([1-9][0-9])|(0[1-9])))$'
                ]
            )
        );
    }

    public function resolveRequest()
    {
        try {
            $context = new RequestContext();
            $request = Request::createFromGlobals();
            $context->fromRequest($request);
            $matcher = new UrlMatcher($this->routes, $context);
            $parameters = $matcher->match($context->getPathInfo());
        }
        catch (ResourceNotFoundException $e) {
            return new Response($e->getMessage(), 404, []);
        }
        try {
            $controllerClass = $parameters['controller'];
            $controllerMethod = $parameters['method'];
            $controller = new $controllerClass();
            return $controller->$controllerMethod($parameters);
        }
        catch (\Exception $e) {
            return new Response($e->getMessage(), 500, []);
        }
    }
}
