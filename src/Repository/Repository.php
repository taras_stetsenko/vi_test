<?php

namespace Repository;

use Entity\Product;
use Entity\Order;
use Utils\Db;

class Repository
{
    private $connection;
    private static $orderStatusIds = [];

    public function __construct()
    {
        $this->connection = Db::getConnection();
        $sql = 'SELECT id, name FROM `orders_status_types`';
        $stmt = $this->connection->prepare($sql);
        $stmt->execute([]);
        while ($row = $stmt->fetch()) {
            self::$orderStatusIds[$row['name']] = (int)$row['id'];
        }
    }

    public function generateProducts() : void
    {
        $this->connection->beginTransaction();
        try {
            $sql = 'INSERT INTO `products` (
                `name`,
                `price`
            ) VALUES (
                :name,
                :price
            )';
            $stmt = $this->connection->prepare($sql);
            for ($i = 1; $i <= 20; $i++) {
                $params = [
                    'name' => 'product_' . ($i),
                    'price' => 100 + $i * 10
                ];
                $stmt->execute($params);
            }
            $this->connection->commit();
        }
        catch (\Exception $e) {
            $this->connection->rollBack();
            throw $e;
        }
    }

    public function getAllProducts() : array
    {
        $sql = 'SELECT
            `id`,
            `name`,
            `price`
        FROM
            `products`';
        $stmt = $this->connection->prepare($sql);
        $stmt->execute([]);
        $res = [];
        while ($row = $stmt->fetch()) {
            $res[] = new Product($row['id'], $row['name'], $row['price']);
        }
        return $res;
    }

    public function createOrder(array $productIds) : int
    {
        if (empty ($productIds)) {
            throw new \Exception(
                'There must be at least one product in an order'
            );
        }
        $this->connection->beginTransaction();
        try {
            $sql = 'INSERT INTO `orders` (
                `status`
            ) VALUES (
                :status_id
            )';
            $stmt = $this->connection->prepare($sql);
            $params = [
                'status_id' => self::$orderStatusIds['new']
            ];
            $stmt->execute($params);
            $orderId = $this->connection->lastInsertId();
            $sql = 'INSERT INTO `orders_products` (
                `order_id`,
                `product_id`
            ) VALUES (
                :order_id,
                :product_id
            )';
            $stmt = $this->connection->prepare($sql);
            foreach ($productIds as $productId) {
                $params = [
                    'order_id' => $orderId,
                    'product_id' => $productId
                ];
                $stmt->execute($params);
            }
            $this->connection->commit();
        }
        catch (\Exception $e) {
            $this->connection->rollBack();
            throw $e;
        }
        return $orderId;
    }

    public function getOrderById(int $orderId) : Order
    {
        if ($orderId == null) {
            throw new \Exception('$orderId must be not null');
        }
        $sql = 'SELECT
            `orders`.`id` AS `order_id`,
            `orders_status_types`.`name` AS `order_status`,
            `products`.`id` AS `product_id`,
            `products`.`name` AS `product_name`,
            `products`.`price` AS `product_price`
        FROM
            `orders`,
            `products`,
            `orders_products`,
            `orders_status_types`
        WHERE
            `orders`.`id` = `orders_products`.`order_id`
            AND `products`.`id` = `orders_products`.`product_id`
            AND `orders_status_types`.`id` = `orders`.`status`
            AND `orders`.`id` = :order_id';
        $stmt = $this->connection->prepare($sql);
        $stmt->execute([
            'order_id' => $orderId
        ]);
        $orderStatus = null;
        $products = [];
        while ($row = $stmt->fetch()) {
            $orderStatus = $row['order_status'];
            $products[] = new Product($row['product_id'], $row['product_name'], $row['product_price']);
        }
        if (empty ($products)) {
            throw new \Exception('Order with id ' . $orderId . ' not found');
        }
        $order = new Order($orderId, $orderStatus, $products);
        return $order;
    }

    public function updateOrderStatus(int $orderId, string $status = 'paid') : void
    {
        if ($orderId == null) {
            throw new \Exception('$orderId must be not null');
        }
        $sql = 'UPDATE `orders`
        SET
            `status` = :status_id
        WHERE
            `id` = :order_id';
        $params = [
            'status_id' => self::$orderStatusIds[$status],
            'order_id' => $orderId
        ];
        $stmt = $this->connection->prepare($sql);
        $stmt->execute($params);
    }
}
