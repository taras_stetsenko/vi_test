<?php
require (__DIR__ . '/../vendor/autoload.php');

use Symfony\Component\HttpFoundation\Response;

use Controller\Router;

try {
    $router = new Router();
    $response = $router->resolveRequest();
    $response->send();
}
catch (\Exception $e) {
    $response = new Response($e->getMessage(), 500);
    $response->send();
}
